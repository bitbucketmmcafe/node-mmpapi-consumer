/** node-mmapi-consumer */

"use strict";

let http = require('http'),
	Q = require('q');

/**
 * APIConsumer
 */
class APIConsumer {

	/**
	 * 
	 * constructor
	 */
	constructor() {
		this._config = null;
	}

	/**
	 * get the configuration
	 * @returns {object} - the configuration object
	 */
	get config() {
		return this._config;
	}

	/**
	 * set the configuration 
	 * @param {object} new_config - host: string, port: string/number
	 */
	set config(new_config) {
		this._config = new_config;
	}

	/**
	 * Using the config info, request a 'path' with the session id provided by the API
	 * @param {string} path - the endpointed to be requested
	 * @param {string} session_id - the current session id provided by the API
	 * @returns {promise} - a promise with the response
	 */
	_requester(path, session_id) {

		let deferred = Q.defer();
		let options = {
			host: this._config.mmpublish.host,
			port: this._config.mmpublish.port,
			path,
			headers: { Cookie: " JSESSIONID=" + (session_id || "") }
		}

		let request = http.request(options, response => {
			let str = ''
			response.on('data', chunk => {
				str += chunk;
			});

			response.on('end', () => {

				if(response.statusCode == 200) {
					try {
						let parsed = JSON.parse(str);
						deferred.resolve(parsed);
					} catch(e) {
						deferred.reject({message: "No valid JSON response", status: 401});	
					}
				} else {
					deferred.reject({message: "Unable to process request", status: response.statusCode, err: response});
				}
			
			});
		});

		request.end();

		return deferred.promise;
	}

	/**
	 * Log in to the API
	 * @param {string} name - the user name
	 * @param {string} pwd - the user password
	 * @returns {promise} - a promise with the response, including the session id
	 */
	login(name, pwd) {
		let path = `/api/v1/login/portal.login.action?login=true&username=${name}&password=${pwd}`;
		return this._requester(path);
	}

	/**
	 * Validate the 'logged user'
	 * @param {string} session_id - the current session id
	 * @returns {promise} - a promise with the response
	 */
	validate(session_id) {

		let path = '/?template=api/v1/profile';
		return this._requester(path, session_id);

	}

	/**
	 * Get the 'logged user' contects
	 * @param {string} session_id - the current session id
	 * @returns {promise} - a promise with the response
	 */
	user_contacts(session_id) {
		let path = '/?template=api/v1/profile/contacts';
		return this._requester(path, session_id);
	}

	/**
	 * Get the profile from a user
	 * @param {string} session_id - the current session id
	 * @param {int} user_id - the id of the desired user profile
	 * @returns {promise} - a promise with the response
	 */
	profile(session_id, user_id) {
		let path = `/api/v1/profile/user.view?userId=${user_id}`;
		return this._requester(path, session_id);
	}

	/**
	 * Get the relations from a user
	 * @param {string} session_id - the current session id
	 * @param {int} user_id - the id of the desired user relations
	 * @returns {promise} - a promise with the response
	 */
	relations(session_id, user_id) {
		let path = `/api/v1/profile/relations/user.view?userId=${user_id}`;
		return this._requester(path, session_id);
	}

	/**
	 * Get multiple users by its ids
	 * @param {string} session_id - the current session id
	 * @param {array} user_ids - the ids of the desired users
	 * @returns {promise} - a promise with the response
	 */
	users_by_ids(session_id, user_ids) {
		let path = `/?template=api/v1/profile/by_user_ids&ids=${user_ids.toString()}`;
		return this._requester(path, session_id);
	}

}

let api_consumer = new APIConsumer();

module.exports = { api_consumer: api_consumer };