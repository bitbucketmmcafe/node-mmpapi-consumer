<a name="APIConsumer"></a>

## APIConsumer
APIConsumer

**Kind**: global class  

* [APIConsumer](#APIConsumer)
    * [new APIConsumer()](#new_APIConsumer_new)
    * [.config](#APIConsumer+config) ⇒ <code>object</code>
    * [.config](#APIConsumer+config)
    * [._requester(path, session_id)](#APIConsumer+_requester) ⇒ <code>promise</code>
    * [.login(name, pwd)](#APIConsumer+login) ⇒ <code>promise</code>
    * [.validate(session_id)](#APIConsumer+validate) ⇒ <code>promise</code>
    * [.user_contacts(session_id)](#APIConsumer+user_contacts) ⇒ <code>promise</code>
    * [.profile(session_id, user_id)](#APIConsumer+profile) ⇒ <code>promise</code>
    * [.relations(session_id, user_id)](#APIConsumer+relations) ⇒ <code>promise</code>

<a name="new_APIConsumer_new"></a>

### new APIConsumer()
constructor

<a name="APIConsumer+config"></a>

### apiConsumer.config ⇒ <code>object</code>
get the configuration

**Kind**: instance property of <code>[APIConsumer](#APIConsumer)</code>  
**Returns**: <code>object</code> - - the configuration object  
<a name="APIConsumer+config"></a>

### apiConsumer.config
set the configuration

**Kind**: instance property of <code>[APIConsumer](#APIConsumer)</code>  

| Param | Type | Description |
| --- | --- | --- |
| new_config | <code>object</code> | host: string, port: string/number |

<a name="APIConsumer+_requester"></a>

### apiConsumer._requester(path, session_id) ⇒ <code>promise</code>
Using the config info, request a 'path' with the session id provided by the API

**Kind**: instance method of <code>[APIConsumer](#APIConsumer)</code>  
**Returns**: <code>promise</code> - - a promise with the response  

| Param | Type | Description |
| --- | --- | --- |
| path | <code>string</code> | the endpointed to be requested |
| session_id | <code>string</code> | the current session id provided by the API |

<a name="APIConsumer+login"></a>

### apiConsumer.login(name, pwd) ⇒ <code>promise</code>
Log in to the API

**Kind**: instance method of <code>[APIConsumer](#APIConsumer)</code>  
**Returns**: <code>promise</code> - - a promise with the response, including the session id  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | the user name |
| pwd | <code>string</code> | the user password |

<a name="APIConsumer+validate"></a>

### apiConsumer.validate(session_id) ⇒ <code>promise</code>
Validate the 'logged user'

**Kind**: instance method of <code>[APIConsumer](#APIConsumer)</code>  
**Returns**: <code>promise</code> - - a promise with the response  

| Param | Type | Description |
| --- | --- | --- |
| session_id | <code>string</code> | the current session id |

<a name="APIConsumer+user_contacts"></a>

### apiConsumer.user_contacts(session_id) ⇒ <code>promise</code>
Get the 'logged user' contects

**Kind**: instance method of <code>[APIConsumer](#APIConsumer)</code>  
**Returns**: <code>promise</code> - - a promise with the response  

| Param | Type | Description |
| --- | --- | --- |
| session_id | <code>string</code> | the current session id |

<a name="APIConsumer+profile"></a>

### apiConsumer.profile(session_id, user_id) ⇒ <code>promise</code>
Get the profile from a user

**Kind**: instance method of <code>[APIConsumer](#APIConsumer)</code>  
**Returns**: <code>promise</code> - - a promise with the response  

| Param | Type | Description |
| --- | --- | --- |
| session_id | <code>string</code> | the current session id |
| user_id | <code>int</code> | the id of the desired user profile |

<a name="APIConsumer+relations"></a>

### apiConsumer.relations(session_id, user_id) ⇒ <code>promise</code>
Get the relations from a user

**Kind**: instance method of <code>[APIConsumer](#APIConsumer)</code>  
**Returns**: <code>promise</code> - - a promise with the response  

| Param | Type | Description |
| --- | --- | --- |
| session_id | <code>string</code> | the current session id |
| user_id | <code>int</code> | the id of the desired user relations |

