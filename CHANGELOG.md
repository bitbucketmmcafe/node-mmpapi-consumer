
#### [Current]
 * [2633a05](../../commit/2633a05) - __(Christian Benseler)__ CHANGELOG and API doc automatically
 * [333ab14](../../commit/333ab14) - __(Christian Benseler)__ New endpoint: relations
 * [e9263d5](../../commit/e9263d5) - __(Christian Benseler)__ New endpoint: profile
 * [50f7354](../../commit/50f7354) - __(Christian Benseler)__ README.md edited online with Bitbucket
 * [f03a761](../../commit/f03a761) - __(Christian Benseler)__ readme links
 * [d14c43f](../../commit/d14c43f) - __(Christian Benseler)__ changelog updated
 * [6be8e10](../../commit/6be8e10) - __(Christian Benseler)__ changelog
 * [ecb4e9d](../../commit/ecb4e9d) - __(Christian Benseler)__ docs generated with npm
 * [76272f8](../../commit/76272f8) - __(Christian Benseler)__ documentation
 * [03affad](../../commit/03affad) - __(Christian Benseler)__ Login wrapper added + initial tests
 * [a5da306](../../commit/a5da306) - __(Christian Benseler)__ some es6 syntax

#### v0.0.2


#### v0.0.1
 * [c9b0b48](../../commit/c9b0b48) - __(Christian Benseler)__ changelog
 * [a5c1fad](../../commit/a5c1fad) - __(Christian Benseler)__ fix config
 * [5d76ee3](../../commit/5d76ee3) - __(Christian Benseler)__ fix config
 * [f6f9b08](../../commit/f6f9b08) - __(Christian Benseler)__ fix config
 * [9639dec](../../commit/9639dec) - __(Christian Benseler)__ first commit
