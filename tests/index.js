"use strict";

let api_consumer = require('../index').api_consumer;

api_consumer.config = {
	mmpublish: {
		host: "localhost",
		port: 8080
	}
};

let token = null;

api_consumer
.login("admin", "admin")
.then( response => {
	console.log("Login: success");
	token = response.token;

	logged_api();
})
.fail( response => {
	console.log("Login: error", response.message, response.status);
});

let logged_api = () => {

	api_consumer
	.validate(token)
	.then( response => {
		console.log("Validate: success");
	})
	.fail( response => {
		console.log("Validate: error");
	});

	api_consumer
	.user_contacts(token)
	.then( response => {
		console.log("User contacts: success - " + response.contacts.length);
	})
	.fail( response => {
		console.log("User contacts: error");
	});

	api_consumer
	.profile(token, 1)
	.then( response => {
		console.log("Profile: success - " + response.id);
	})
	.fail( response => {
		console.log("Profile: error");
	});

	api_consumer
	.relations(token, 1)
	.then( response => {
		console.log("Relations: success");
	})
	.fail( response => {
		console.log("Relations: error");
	});

	api_consumer
	.users_by_ids(token, [1])
	.then( response => {
		console.log("Users by id: success - " + response.length + " user found");
	})
	.fail( response => {
		console.log("Users by id: error");
	});

}
